##  Pilares de la Programación Orientada a Objetos (POO

Este proyecto en C# demuestra los cuatro pilares de la Programación Orientada a Objetos (POO): Encapsulamiento, Abstracción, Herencia y Polimorfismo. A continuación, se presenta una breve descripción de cada componente clave:

## 1. **Interfaz `IJuguete`**

- **Propósito:** Define un contrato para objetos que se consideran juguetes.
- **Métodos:**
  - `Rodar()`: Describe cómo un juguete puede rodar.
  - `Rebotar()`: Define la acción de rebote para juguetes.

## 2. **Clase `Juguete`**

- **Propósito:** Sirve como una clase base abstracta para todos los juguetes.
- **Métodos:**
  - `Jugar()`: Imprime en consola un mensaje genérico indicando que el juguete está en acción.

## 3. **Clase `Pelota`**

- **Propósito:** Hereda de `Juguete`, implementa `IJuguete` y muestra el uso de encapsulamiento.
- **Propiedades:**
  - `Color`
  - `Volumen`
  - `Dureza`
  - `Peso`
- **Métodos:**
  - `Rodar()`: Imprime un mensaje indicando que la pelota está rodando.
  - `Rebotar()`: Imprime un mensaje indicando que la pelota está rebotando.
  - `Jugar()`: Sobrescribe el método base para mostrar información específica de la pelota, incluido su color.

## 4. **Clases `PelotaBaloncesto` y `PelotaFutbol`**

- **Propósito:** Ejemplos de herencia, extendiendo la funcionalidad de la clase base `Pelota`.

## 5. **Programa Principal (`Program`)**

- **Propósito:** Muestra cómo se pueden utilizar objetos de diferentes tipos de manera uniforme gracias al polimorfismo.
- **Método:**
  - `Main()`: Crea instancias de diferentes tipos de pelotas y llama a sus métodos para demostrar el polimorfismo.

Este proyecto destaca la aplicación práctica de los pilares de la POO, proporcionando una estructura clara y eficiente para modelar el comportamiento de juguetes, específicamente pelotas, en un entorno de programación orientada a objetos.O, proporcionando una estructura clara y eficiente para modelar el comportamiento de juguetes, específicamente pelotas, en un entorno de programación orientada a objetos.