﻿namespace pilaresPOO
{
    internal class Program
    {
        static void Main()
        {
            Pelota miPelota = new("Rojo", 15.0, 5, 0.3);
            miPelota.Rodar();
            miPelota.Rebotar();
            miPelota.Jugar();

            PelotaFutbol pelotaFutbol = new("Blanco y Negro", 14.0, 4, 0.4);
            pelotaFutbol.Rodar();
            pelotaFutbol.Rebotar();
            pelotaFutbol.Jugar();

            PelotaBaloncesto pelotaBaloncesto = new("Naranja", 16.0, 6, 0.5);
            pelotaBaloncesto.Rodar();
            pelotaBaloncesto.Rebotar();
            pelotaBaloncesto.Jugar();
        }
    }
}
