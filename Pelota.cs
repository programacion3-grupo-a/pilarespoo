﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pilaresPOO
{
    public class Pelota : Juguete, IJuguete
    {
        public string Color { get; set; }
        public double Volumen { get; set; }
        public int Dureza { get; set; }
        public double Peso { get; set; }

        public Pelota(string color, double volumen, int dureza, double peso)
        {
            Color = color;
            Volumen = volumen;
            Dureza = dureza;
            Peso = peso;
        }

        public void Rodar()
        {
            Console.WriteLine("La pelota está rodando.");
        }

        public void Rebotar()
        {
            Console.WriteLine("La pelota está rebotando.");
        }

        public new void Jugar()
        {
            Console.WriteLine($"¡La pelota de color {Color} está en acción!");
        }
    }
}
