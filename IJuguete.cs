﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pilaresPOO
{
    public interface IJuguete
    {
        void Rodar();
        void Rebotar();
    }
}
